package com.repusic.app.lottogenerator.boundary;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Mijo Repušić
 */
public class LottoGeneratorService {

    public static final int DEFAULT_NUM_TICKETS = 6;
    public static final int DEFAULT_MAX_VALUE = 49;
    public static final int DEFAULT_NUM_VALUES = 6;
    private Random random = new Random(System.currentTimeMillis());
    
    private Set<Integer> alreadyUsed =  new TreeSet<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LottoGeneratorService lottoTicket = new LottoGeneratorService();
        for (int i = 0; i < DEFAULT_NUM_VALUES; ++i) {
            System.out.println(lottoTicket.getNumbers(DEFAULT_NUM_VALUES, DEFAULT_MAX_VALUE));
        }
    }
    
    public Set<Integer> getNumbers(int numValues, int maxValue) {
        final Set<Integer> numbers = new TreeSet<>();
        while (numbers.size() < numValues) {
            Integer result;
            do {
                result = this.random.nextInt(maxValue) + 1;
            } while (alreadyUsed.contains(result));
            alreadyUsed.add(result);
            numbers.add(result);
        }
        return numbers;
    }
    
}
